---
title: Sweetened Condensed Milk Cake
subtitle: test
date: 2019-04-15
slug: condensed-milk-cake
author:Andrija Jovanovic
---

__Ingredients__

+ 1 can (395g) sweetened condensed milk
+ 4 eggs
+ 120gm all purpose flour
+ 1/2 tblsp baking powder
+ 50gm unsalted butter, melted and cooled
+ Icing sugar for dusting

__Preparation__

Preheat oven to 180C. Generously butter a 22cm ring cake pan.
 
Place all ingredients in a large bowl and mix well. Pour the batter into prepared pan and bake for 20 minutes or until a skewer comes out clean. 
 
Remove from oven and place on a wire rack to cool completely before unmolding.
 
Dust with icing sugar before serving.

