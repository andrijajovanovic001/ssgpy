---
title: Get started With qtile
subtitle:window manager configured in python
author: Andrija Jovnaovic
date: 2020-04-26
slug: qtile-intro
---

__What is Qtile?__

Qtile is simple Window manager written and configured in python [see more](https://qtile.org)

__Installing Qtile__

To install qtile use following:

for arch and arch based distros

```
    sudo pacman -S qtile
```

for debian and debian based distros

```
    sudo apt-get install qtile
```
